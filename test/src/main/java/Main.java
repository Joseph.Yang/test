import app.DemoServiceApp;

/**
 * @author jie
 */
public class Main {
    public static void main(String[] args) {

        new DemoServiceApp().start();

    }
}
