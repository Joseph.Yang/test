package app.demo.domain;

import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

/**
 * @author jie
 */

@Table(name = "user_infos")
public class Student {
    @PrimaryKey(autoIncrement = true)
    @Column(name = "id")
    public long id;

    @Column(name = "user_name")
    public String userName;

    @Column(name = "password")
    public String password;
}
