package app.demo.service;

import core.framework.inject.Inject;
import core.framework.log.Markers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jie
 */
public class Painter {
    private final Logger logger = LoggerFactory.getLogger(Painter.class);
    @Inject
    Brush brush;

    public void draw() {
        logger.warn(Markers.errorCode("DRAW_ERROR"), "drawing picture");
        brush.print();
    }
}
