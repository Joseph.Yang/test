package app.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jie
 */
public class Brush {
    private final Logger logger = LoggerFactory.getLogger(Brush.class);

    public void print() {
        logger.warn("Hello World");
    }
}
