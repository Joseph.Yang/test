package app;

import app.demo.service.Brush;
import app.demo.service.Painter;
import core.framework.module.App;


/**
 * @author jie
 */
public class DemoServiceApp extends App {

    @Override
    protected void initialize() {
        bind(Brush.class);
        bind(Painter.class);
        bean(Painter.class).draw();
    }
}
