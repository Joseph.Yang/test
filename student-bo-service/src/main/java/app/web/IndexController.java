package app.web;

import core.framework.log.Markers;
import core.framework.web.Request;
import core.framework.web.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author joseph
 */
public class IndexController {
    private final Logger logger = LoggerFactory.getLogger(IndexController.class);

    public Response index(Request request) {
        IndexPage indexPage = new IndexPage();
        indexPage.name = "index";
        return Response.html("/template/index.html", indexPage, "en_US");
    }

    public Response submit(Request request) {
        logger.warn("test", Markers.errorCode("TEST_ERROR_CODE"));
        return Response.text("hello " + request.formParams().getOrDefault("name", "nobody"));
    }

}
