package app.web.handler;

import core.framework.log.Markers;
import core.framework.web.ErrorHandler;
import core.framework.web.Request;
import core.framework.web.Response;
import core.framework.web.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * @author joseph
 */
public class TestErrorHandler implements ErrorHandler {
    private final Logger logger = LoggerFactory.getLogger(TestErrorHandler.class);

    @Override
    public Optional<Response> handle(Request request, Throwable e) {
        if (e instanceof NotFoundException){
            logger.warn("TEST_ERROR");
        }
        return Optional.empty();
    }
}
