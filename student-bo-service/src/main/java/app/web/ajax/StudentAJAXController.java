package app.web.ajax;

import app.demo.api.StudentWebService;
import app.demo.api.student.SearchStudentView;
import core.framework.impl.log.ActionLog;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;
import core.framework.util.StopWatch;
import core.framework.web.Request;
import core.framework.web.Response;

import java.util.Map;

public class StudentAJAXController {
    @Inject
    StudentWebService studentWebService;

    public Response searchStudent(Request request) {
        Map<String, String> map = request.queryParams();
        String id = map.get("id");
        StopWatch stopWatch = new StopWatch();
        try {
            SearchStudentView searchStudentView = studentWebService.get(Integer.valueOf(id));
            return Response.bean(searchStudentView);
        }finally {
            ActionLogContext.put("getElapsed",stopWatch.elapsed());
        }
    }
}
