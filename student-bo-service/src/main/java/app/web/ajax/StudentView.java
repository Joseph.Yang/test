package app.web.ajax;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class StudentView {
    @NotNull
    @Property(name = "id")
    public Integer id;

    @NotNull
    @NotBlank
    @Property(name = "name")
    public String name;

    @NotNull
    @Property(name = "age")
    public Integer age;
}
