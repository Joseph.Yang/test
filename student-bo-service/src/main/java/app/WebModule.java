package app;

import app.demo.api.StudentWebService;
import app.domain.Student;
import app.web.IndexController;
import app.web.IndexPage;
import app.web.ajax.StudentAJAXController;
import app.web.ajax.StudentView;
import app.web.handler.TestErrorHandler;
import core.framework.module.HTTPConfig;
import core.framework.module.Module;

import java.util.List;

import static core.framework.http.HTTPMethod.GET;
import static core.framework.http.HTTPMethod.POST;

public class WebModule extends Module {
    @Override
    protected void initialize() {
        db().repository(Student.class);
        http().httpPort(Integer.parseInt(requiredProperty("sys.http.port")));

        TestErrorHandler testErrorHandler = bind(TestErrorHandler.class);
        HTTPConfig http = http();
        http.errorHandler(testErrorHandler);

        List<String> messages = List.of("messages/main.properties", "messages/main_en.properties", "messages/main_en_US.properties");
        site().message(messages, "en_US");
        site().template("/template/index.html", IndexPage.class);
        api().client(StudentWebService.class, requiredProperty("app.user.service.url"));

        IndexController indexController = bind(IndexController.class);
        http().route(GET, "/", indexController::index);
        http().route(GET, "/submit", indexController::submit);


        http().bean(StudentView.class);
        StudentAJAXController studentAJAXController = bind(StudentAJAXController.class);
        http().route(GET, "/ajax/student/id", studentAJAXController::searchStudent);

    }
}
