package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

public class DemoSiteApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));
        load(new WebModule());
    }
}
