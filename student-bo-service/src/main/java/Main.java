import app.DemoSiteApp;

/**
 * @author joseph
 */
public class Main {
    public static void main(String[] args) {
        new DemoSiteApp().start();
    }
}
