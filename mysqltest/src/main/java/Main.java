import app.MysqlServiceApp;

/**
 * @author jie
 */
public class Main {
    public static void main(String[] args) {
        new MysqlServiceApp().start();
    }
}
