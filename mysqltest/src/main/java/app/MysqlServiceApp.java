package app;

import app.demo.service.UserInfoService;
import core.framework.module.App;
import core.framework.module.SystemModule;


/**
 * @author jie
 */
public class MysqlServiceApp extends App {
    @Override
    protected void initialize() {

        load(new SystemModule("sys.properties"));
        load(new UserInfoModule());
        onStartup(() -> {
            UserInfoService userInfoService = bean(UserInfoService.class);
            userInfoService.searchUserInfoById(1);
            userInfoService.searchUserInfosByCondition("s", null);
            userInfoService.delete(1);
        });
    }
}
