package app.demo.domain;

import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

/**
 * @author jie
 */

@Table(name = "user_infos")
public class UserInfo {
    @PrimaryKey(autoIncrement = true)
    @Column(name = "user_id")
    public Integer id;

    @NotNull
    @NotBlank
    @Column(name = "user_name")
    public String userName;

    @NotNull
    @NotBlank
    @Column(name = "user_phone")
    public String userPhone;

    @NotNull
    @NotBlank
    @Column(name = "password")
    public String password;

    @Column(name = "user_img")
    public String userImg;

}
