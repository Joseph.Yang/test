package app.demo.service;

import app.demo.domain.UserInfo;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;
import core.framework.web.exception.NotFoundException;

import java.util.List;

/**
 * @author jie
 */
public class UserInfoService {
    @Inject
    Repository<UserInfo> userInfoRepository;

    public void create(UserInfo userInfo) {
        (userInfoRepository.selectOne("user_name = ? AND user_phone = ?", userInfo.userName, userInfo.userPhone)).orElseThrow(
            () -> new NotFoundException("User not found"));
        UserInfo user = new UserInfo();
        user.userName = userInfo.userName;
        user.password = userInfo.password;
        user.userPhone = userInfo.userPhone;
        userInfoRepository.insert(user);
    }

    public void delete(Integer userId) {
        userInfoRepository.get(userId).orElseThrow(() -> new NotFoundException("User not found, id=" + userId));
        userInfoRepository.delete(userId);
    }

    public void update(UserInfo userInfo) {
        userInfoRepository.get(userInfo.id).orElseThrow(() -> new NotFoundException("User not found"));
        userInfoRepository.partialUpdate(userInfo);
    }

    public UserInfo searchUserInfoById(Integer userId) {
        return userInfoRepository.get(userId).orElseThrow(() -> new NotFoundException("User not found, id=" + userId));
    }

    public List<UserInfo> searchUserInfosByCondition(String nameKey, String phoneKey) {
        Query<UserInfo> query = userInfoRepository.select();
        if (!Strings.isBlank(nameKey))
            query.where("user_name LIKE ?", Strings.format("%{}%", nameKey));
        if (!Strings.isBlank(phoneKey))
            query.where("user_phone LIKE ?", Strings.format("%{}%", phoneKey));
        return query.fetch();
    }
}
