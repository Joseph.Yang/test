package app;

import app.demo.domain.UserInfo;
import app.demo.service.UserInfoService;
import core.framework.module.Module;

/**
 * @author jie
 */
public class UserInfoModule extends Module {

    @Override
    protected void initialize() {
        db().repository(UserInfo.class);
        bind(UserInfoService.class);

    }
}
