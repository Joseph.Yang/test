import app.SubscribeApp;

/**
 * @author joseph
 */
public class Main {
    public static void main(String[] args) {
        new SubscribeApp().start();
    }
}
