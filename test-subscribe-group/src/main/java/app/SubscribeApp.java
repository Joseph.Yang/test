package app;

import app.handler.TestMessageHandler;
import app.message.api.TestMessage;
import core.framework.module.App;
import core.framework.module.KafkaConfig;
import core.framework.module.SystemModule;

/**
 * @author joseph
 */
public class SubscribeApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));

        KafkaConfig kafkaConfig = kafka();
        //kafkaConfig.uri(requiredProperty("sys.kafka.uri"));
        kafkaConfig.groupId("group1");
        kafkaConfig.subscribe("topic1", TestMessage.class, bind(TestMessageHandler.class));
    }
}
