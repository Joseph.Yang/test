CREATE TABLE IF NOT EXISTS `teachers`  (
  `id`   int(11)       NOT NULL AUTO_INCREMENT,
  `name` varchar(255)  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `age`  int(11)       NOT NULL,
  PRIMARY KEY (`id`)
)