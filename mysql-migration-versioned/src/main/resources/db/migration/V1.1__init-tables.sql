CREATE TABLE IF NOT EXISTS `teachers` (
  `id`         INT(11)                                                      NOT NULL,AUTO_INCREMENT,
  `name`       VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci      NOT NULL,
  `age`        INT(11)                                                      NOT NULL,
  `course`     ENUM ('MATH', 'CHINESE','ENGLISH')                           NOT NULL
  PRIMARY KEY (`id`)
);
