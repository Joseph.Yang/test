package app.demo.api;

import app.demo.api.student.CreateStudentRequest;
import app.demo.api.student.CreateStudentView;
import app.demo.api.student.SearchStudentRequest;
import app.demo.api.student.SearchStudentResponse;
import app.demo.api.student.SearchStudentView;
import app.demo.api.student.UpdateStudentRequest;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author joseph
 */
public interface StudentWebService {
    @GET
    @Path("/student/:id")
    SearchStudentView get(@PathParam("id") Integer id);

    @POST
    @Path("/student")
    @ResponseStatus(HTTPStatus.CREATED)
    CreateStudentView create(CreateStudentRequest request);

    @PUT
    @Path("/student/:id")
    void update(@PathParam("id") Integer id, UpdateStudentRequest request);

    @GET
    @Path("/student/search")
    SearchStudentResponse search(SearchStudentRequest request);
}
