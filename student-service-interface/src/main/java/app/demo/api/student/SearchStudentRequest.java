package app.demo.api.student;

import core.framework.api.validate.Min;
import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author joseph
 */
public class SearchStudentRequest {
    @NotNull
    @Min(0)
    @QueryParam(name = "skip")
    public Integer skip;

    @NotNull
    @Min(5)
    @QueryParam(name = "limit")
    public Integer limit;

    @QueryParam(name = "name")
    public String name;

    @QueryParam(name = "age")
    public Integer age;
}
