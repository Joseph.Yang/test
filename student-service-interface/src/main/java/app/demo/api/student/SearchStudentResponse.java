package app.demo.api.student;

import core.framework.api.json.Property;

import java.util.List;

/**
 * @author joseph
 */
public class SearchStudentResponse {
    @Property(name = "students")
    public List<SearchStudentView> students;

    //todo total
    @Property(name = "total")
    public Integer total;
}
