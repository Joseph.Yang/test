package app.demo.api.student;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class CreateStudentView {
    @NotNull
    @Property(name = "id")
    public Integer id;

    @NotNull
    @Property(name = "name")
    public String name;

    @NotNull
    @Property(name = "age")
    public Integer age;
}
