package app.job;

import app.service.PublisherService;
import core.framework.inject.Inject;
import core.framework.scheduler.Job;

/**
 * @author joseph
 */
public class SchedulerJob implements Job {
    @Inject
    PublisherService publisherService;

    @Override
    public void execute() {
        publisherService.publisher();
    }
}
