package app.service;

import app.message.api.TestMessage;
import core.framework.inject.Inject;
import core.framework.kafka.MessagePublisher;

/**
 * @author joseph
 */
public class PublisherService {
    @Inject
    MessagePublisher<TestMessage> messagePublisher;

    public void publisher() {
        for (int i = 0; i < 10; i++) {
            TestMessage testMessage = new TestMessage();
            testMessage.message = "hello";
            messagePublisher.publish(testMessage);
        }
    }
}
