package app;

import app.message.api.TestMessage;
import core.framework.module.App;
import core.framework.module.KafkaConfig;
import core.framework.module.SystemModule;

/**
 * @author joseph
 */
public class PublishApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));

        KafkaConfig kafkaConfig = kafka();
        kafkaConfig.publish("topic1", TestMessage.class);

        load(new PublishModule());
    }
}
