package app;

import app.job.SchedulerJob;
import app.service.PublisherService;
import core.framework.module.Module;
import core.framework.module.SchedulerConfig;

import java.time.Duration;

/**
 * @author joseph
 */
public class PublishModule extends Module {

    @Override
    protected void initialize() {
        PublisherService publisherService = bind(PublisherService.class);
        publisherService.publisher();

        SchedulerConfig schedulerConfig = schedule();
        SchedulerJob schedulerJob = bind(SchedulerJob.class);
        schedulerConfig.fixedRate("fixed-rate", schedulerJob, Duration.ofSeconds(5));
    }
}
