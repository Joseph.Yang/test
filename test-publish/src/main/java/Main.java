import app.PublishApp;

/**
 * @author joseph
 */
public class Main {
    public static void main(String[] args) {
        new PublishApp().start();
    }
}
