package app.message.api;

import core.framework.api.json.Property;

/**
 * @author joseph
 */
public class TestMessage {
    @Property(name = "message")
    public String message;
}
