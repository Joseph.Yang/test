package app;

import core.framework.module.App;

/**
 * @author joseph
 */
public class DemoApp extends App {
    @Override
    protected void initialize() {
        load(new DemoModule());
    }
}
