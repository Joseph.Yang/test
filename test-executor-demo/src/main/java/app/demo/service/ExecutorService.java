package app.demo.service;

import app.demo.task.ExecutorDelayTask;
import app.demo.task.ExecutorTask;
import core.framework.async.Executor;
import core.framework.inject.Inject;
import core.framework.inject.Named;

import java.time.Duration;

/**
 * @author joseph
 */
public class ExecutorService {
    @Inject
    Executor executor;

    @Inject
    @Named("demo")
    Executor demoExecutor;

    public void executor() {
        executor.submit("async", new ExecutorTask());
        demoExecutor.submit("async", new ExecutorDelayTask(), Duration.ofSeconds(10));
    }


}
