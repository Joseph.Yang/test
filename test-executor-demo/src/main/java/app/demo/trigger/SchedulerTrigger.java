package app.demo.trigger;

import core.framework.scheduler.Trigger;
import core.framework.util.Randoms;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class  SchedulerTrigger implements Trigger {
    @Override
    public ZonedDateTime next(ZonedDateTime previous) {
        return previous.plusSeconds(Randoms.nextInt(7, 13));
    }
}
