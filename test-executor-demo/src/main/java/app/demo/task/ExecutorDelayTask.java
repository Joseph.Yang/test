package app.demo.task;

import core.framework.async.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author joseph
 */
public class ExecutorDelayTask implements Task {
    private final Logger logger = LoggerFactory.getLogger(ExecutorDelayTask.class);

    @Override
    public void execute() throws Exception {
        logger.warn("delay executor demo");
    }
}
