package app.demo.task;

import core.framework.async.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author joseph
 */
public class ExecutorTask implements Task {
    private final Logger logger = LoggerFactory.getLogger(ExecutorTask.class);

    @Override
    public void execute() throws Exception {
        logger.warn("executor demo");
    }
}
