package app.demo.job;

import app.demo.task.ExecutorTask;
import core.framework.async.Executor;
import core.framework.inject.Inject;
import core.framework.scheduler.Job;

/**
 * @author joseph
 */
public class TriggerJob implements Job {
    @Inject
    Executor executor;

    ExecutorTask executorTask = new ExecutorTask();

    @Override
    public void execute() throws Exception {
        executor.submit("async", () -> {
            executorTask.execute();
        });
    }
}
