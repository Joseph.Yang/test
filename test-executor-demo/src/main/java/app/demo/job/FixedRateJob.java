package app.demo.job;

import app.demo.service.ExecutorService;
import core.framework.async.Executor;
import core.framework.inject.Inject;
import core.framework.scheduler.Job;

/**
 * @author joseph
 */
public class FixedRateJob implements Job {
    @Inject
    Executor executor;

    ExecutorService executorService = new ExecutorService();

    @Override
    public void execute() throws Exception {
        executor.submit("async", () -> {
            executorService.executor();
        });
    }
}
