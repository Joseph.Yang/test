package app;

import app.demo.job.FixedRateJob;
import app.demo.job.TriggerJob;
import app.demo.service.ExecutorService;
import app.demo.trigger.SchedulerTrigger;
import core.framework.module.ExecutorConfig;
import core.framework.module.Module;
import core.framework.module.SchedulerConfig;

import java.time.Duration;

/**
 * @author joseph
 */
public class DemoModule extends Module {
    @Override
    protected void initialize() {
        ExecutorConfig executorConfig = executor();
        executorConfig.add();
        executorConfig.add("demo", 3);

        ExecutorService executorService = bind(ExecutorService.class);
        onStartup(() -> {
            context.logManager.begin("async");
            executorService.executor();
            context.logManager.end("async");
        });

        SchedulerConfig schedulerConfig = schedule();
        FixedRateJob fixedRateJob = bind(FixedRateJob.class);
        schedulerConfig.fixedRate("fixedRate", fixedRateJob, Duration.ofSeconds(5));

        TriggerJob triggerJob = bind(TriggerJob.class);
        SchedulerTrigger schedulerTrigger = bind(SchedulerTrigger.class);
        schedulerConfig.trigger("trigger", triggerJob, schedulerTrigger);
    }
}
