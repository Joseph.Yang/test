import app.DemoApp;

/**
 * @author joseph
 */
public class Main {
    public static void main(String[] args) {
        new DemoApp().start();
    }
}
