package app.demo.student.service;

import app.demo.IntegrationTest;
import app.demo.api.student.CreateStudentRequest;
import app.demo.api.student.CreateStudentView;
import app.demo.api.student.SearchStudentRequest;
import app.demo.api.student.SearchStudentResponse;
import app.demo.api.student.SearchStudentView;
import app.demo.api.student.UpdateStudentRequest;
import core.framework.inject.Inject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author joseph
 */
class StudentServiceTest extends IntegrationTest {
    private static Integer id = 0;

    @Inject
    private StudentService studentService;

    @BeforeEach
    void creatStudentTest() {
        CreateStudentRequest studentTest = new CreateStudentRequest();
        studentTest.name = "studentTest1";
        studentTest.age = 1;
        CreateStudentView createStudentView = studentService.create(studentTest);
        id = createStudentView.id;
    }

    @AfterEach
    void clear(){
        studentService.delete(id);
    }

    @Test
    void get() {
        SearchStudentView studentView = studentService.get(id);
        assertThat(studentView.name).isEqualTo("studentTest1");
    }

    @Test
    void create() {
        var request = new CreateStudentRequest();
        request.name = "studentTest3";
        request.age = 3;

        CreateStudentView createStudentView = studentService.create(request);
        assertThat(createStudentView.id).isNotNull();
        assertThat(studentService.get(createStudentView.id)).satisfies(student -> {
            assertThat(student.name).isEqualTo(request.name);
            assertThat(student.age).isEqualTo(request.age);
        });
    }

    @Test
    void update() {
        UpdateStudentRequest request = new UpdateStudentRequest();
        request.name = "update2";
        request.age = 11;
        studentService.update(id, request);

        assertThat(id).isNotNull();
        assertThat(studentService.get(id)).satisfies(student->{
            assertThat(student.name).isEqualTo(request.name);
        });
    }

    @Test
    void search() {
        CreateStudentView student = createStudent("studentTest2", 2);

        SearchStudentRequest request = new SearchStudentRequest();
        request.name="studentTest2";
        request.skip = 0;
        request.limit = 10;
        SearchStudentResponse search = studentService.search(request);
        assertThat(search.students).hasSize(1).first().isEqualToComparingFieldByField(student);
    }

    private CreateStudentView createStudent(String name, Integer age) {
        CreateStudentRequest studentTest = new CreateStudentRequest();
        studentTest.name = name;
        studentTest.age = age;
        return studentService.create(studentTest);
    }
}