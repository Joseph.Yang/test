import app.DemoServiceApp;

/**
 * @author joseph
 */
public class Main {
    public static void main(String[] args) {
        new DemoServiceApp().start();
    }
}
