package app;

import app.demo.api.StudentWebService;
import app.demo.student.domain.Student;
import app.demo.student.service.StudentService;
import app.demo.student.web.StudentWebServiceImpl;
import core.framework.module.Module;

/**
 * @author joseph
 */
public class StudentModule extends Module {

    @Override
    protected void initialize() {
        db().repository(Student.class);
        bind(StudentService.class);
        api().service(StudentWebService.class, bind(StudentWebServiceImpl.class));
    }
}
