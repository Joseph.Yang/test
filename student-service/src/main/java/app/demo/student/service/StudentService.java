package app.demo.student.service;

import app.demo.api.student.CreateStudentRequest;
import app.demo.api.student.CreateStudentView;
import app.demo.api.student.SearchStudentRequest;
import app.demo.api.student.SearchStudentResponse;
import app.demo.api.student.SearchStudentView;
import app.demo.api.student.UpdateStudentRequest;
import app.demo.student.domain.Student;
import core.framework.api.json.Property;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;
import core.framework.web.exception.NotFoundException;

import java.util.OptionalLong;
import java.util.stream.Collectors;

/**
 * @author joseph
 */
public class StudentService {
    @Inject
    public Repository<Student> studentRepository;

    public SearchStudentView get(Integer id) {
        Student student = studentRepository.get(id).orElseThrow(() -> new NotFoundException("student not found" + id));
        return view(student);
    }

    public CreateStudentView create(CreateStudentRequest request) {
        Student student = new Student();
        student.name = request.name;
        student.age = request.age;
        OptionalLong insert = studentRepository.insert(student);
        int id = (int) insert.getAsLong();
        student.id = id;
        return creatView(student);
    }

    public void update(Integer id, UpdateStudentRequest request) {
        Student student = studentRepository.get(id).orElseThrow(() -> new NotFoundException("student not found" + id));
        student.name = request.name;
        student.age = request.age;
        studentRepository.update(student);
    }

    public SearchStudentResponse search(SearchStudentRequest request) {
        Query<Student> query = studentRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        if (!Strings.isBlank(request.name))
            query.where("name LIKE ?", request.name);
        if (request.age != null)
            query.where("age = ?", request.age);
        SearchStudentResponse searchStudentResponse = new SearchStudentResponse();
        searchStudentResponse.students = query.fetch().stream().map(this::view).collect(Collectors.toList());
        searchStudentResponse.total = query.count();
        return searchStudentResponse;
    }
    public void delete(Integer id){
        Student student = studentRepository.get(id).orElseThrow(() -> new NotFoundException("student not found" + id));
        studentRepository.delete(id);
    }

    private SearchStudentView view(Student student) {
        SearchStudentView studentView = new SearchStudentView();
        studentView.id = student.id;
        studentView.name = student.name;
        studentView.age = student.age;
        return studentView;
    }

    private CreateStudentView creatView(Student student) {
        CreateStudentView createStudentView = new CreateStudentView();
        createStudentView.id = student.id;
        createStudentView.name = student.name;
        createStudentView.age = student.age;
        return createStudentView;
    }
}
