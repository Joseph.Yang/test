package app.demo.student.web;

import app.demo.api.StudentWebService;
import app.demo.api.student.CreateStudentRequest;
import app.demo.api.student.CreateStudentView;
import app.demo.api.student.SearchStudentRequest;
import app.demo.api.student.SearchStudentResponse;
import app.demo.api.student.SearchStudentView;
import app.demo.api.student.UpdateStudentRequest;
import app.demo.student.service.StudentService;
import core.framework.inject.Inject;

/**
 * @author joseph
 */
public class StudentWebServiceImpl implements StudentWebService {
    @Inject
    StudentService studentService;

    @Override
    public SearchStudentView get(Integer id) {
        return studentService.get(id);
    }

    @Override
    public CreateStudentView create(CreateStudentRequest request) {
        return studentService.create(request);
    }

    @Override
    public void update(Integer id, UpdateStudentRequest request) {
        studentService.update(id, request);
    }

    @Override
    public SearchStudentResponse search(SearchStudentRequest request) {
        return studentService.search(request);
    }
}
