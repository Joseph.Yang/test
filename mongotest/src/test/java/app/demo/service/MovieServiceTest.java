package app.demo.service;

import app.demo.IntegrationTest;
import app.demo.domain.CreateMovieRequest;
import app.demo.domain.Movie;
import app.demo.domain.MovieView;
import app.demo.domain.ReplaceMovieRequest;
import app.demo.domain.TestMovie;
import app.demo.domain.UpdateMovieRequest;
import core.framework.inject.Inject;
import core.framework.mongo.MongoCollection;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author joseph
 */
class MovieServiceTest extends IntegrationTest {
    @Inject
    private MovieService movieService;

    @Inject
    MongoCollection<Movie> movieMongoCollection;

    private static String movieId = null;

    @BeforeEach
    void creat() {
        MovieView movie = createMovie("Test", 100);
        movieId = movie.movieId;
    }

    @AfterEach
    void clear() {
        movieMongoCollection.delete(movieId);
    }

    @Test
    void delete() {
        movieService.delete(movieId);
        assertThat(movieMongoCollection.get(movieId)).isEmpty();
    }

    @Test
    void search() {
        MovieView movieView = createMovie("Test2", 200);

        List<MovieView> movies = movieService.search(null, "Test2", null);
        assertThat(movies).hasSize(1).first().isEqualToComparingFieldByField(movieView);
    }

    @Test
    void replace() {
        ReplaceMovieRequest request = new ReplaceMovieRequest();
        request.movieId = new ObjectId().toString();
        request.movieName = "Test3";
        request.movieMinutes = 100;
        movieService.replace(request);

        assertThat(movieMongoCollection.get(request.movieId)).get().isEqualToComparingFieldByField(request);

        request.movieName = "replace";
        movieService.replace(request);

        assertThat(movieMongoCollection.get(request.movieId)).get().isEqualToComparingFieldByField(request);
    }

    @Test
    void update() {
        UpdateMovieRequest request = new UpdateMovieRequest();
        request.movieName = "update";
        movieService.update(movieId, request);
        assertThat(movieMongoCollection.get(movieId)).get().satisfies(movie ->
            assertThat(movie.movieName).isEqualTo("update"));
    }

    @Test
    void insert() {
        CreateMovieRequest request = new CreateMovieRequest();
        request.movieId = new ObjectId().toString();
        request.movieName = "insert";
        request.movieMinutes = 500;
        movieService.insert(request);

        assertThat(request.movieId).isNotBlank();
        assertThat(movieMongoCollection.get(request.movieId)).get().satisfies(movie -> {
            assertThat(movie.movieName).isEqualTo("insert");
            assertThat(movie.movieMinutes).isEqualTo(500);
        });

    }

    private MovieView createMovie(String movieName, Integer movieMinutes) {
        CreateMovieRequest request = new CreateMovieRequest();
        request.movieId = new ObjectId().toString();
        request.movieName = movieName;
        request.movieMinutes = movieMinutes;
        MovieView movieView = movieService.insert(request);
        return movieView;
    }
}