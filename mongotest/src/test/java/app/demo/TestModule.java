package app.demo;

import app.MongoServiceApp;
import app.demo.domain.TestMovie;
import core.framework.mongo.Mongo;
import core.framework.mongo.module.MongoConfig;
import core.framework.test.module.AbstractTestModule;

/**
 * @author neo
 */
public class TestModule extends AbstractTestModule {
    @Override
    protected void initialize() {
        load(new MongoServiceApp());
    }
}
