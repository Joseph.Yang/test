package app;

import app.demo.domain.Movie;
import app.demo.service.MovieService;
import core.framework.module.Module;
import core.framework.mongo.module.MongoConfig;

/**
 * @author jie
 */
public class MovieModule extends Module {
    @Override
    protected void initialize() {
        MongoConfig mongoConfig = config(MongoConfig.class);
        mongoConfig.uri(requiredProperty("sys.mongo.uri"));
        mongoConfig.collection(Movie.class);
        bind(MovieService.class);
    }
}
