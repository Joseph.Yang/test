package app.demo.domain;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

/**
 * @author joseph
 */
public class MovieView {
    @NotNull
    @NotBlank
    @Property(name = "id")
    public String movieId;

    @Property(name = "movie_name")
    public String movieName;

    @Property(name = "movie_minutes")
    public Integer movieMinutes;


    @Property(name = "language")
    public String language;

    @Property(name = "online_time")
    public LocalDateTime onlineTime;
}
