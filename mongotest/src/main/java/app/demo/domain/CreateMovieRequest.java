package app.demo.domain;

import core.framework.api.validate.NotNull;
import core.framework.mongo.Field;
import core.framework.mongo.Id;

import java.time.LocalDateTime;

/**
 * @author joseph
 */
public class CreateMovieRequest {
    @Id
    @Field(name = "id")
    public String movieId;

    @NotNull
    @Field(name = "movie_name")
    public String movieName;

    @NotNull
    @Field(name = "movie_minutes")
    public Integer movieMinutes;

    @Field(name = "language")
    public String language;

    @Field(name = "online_time")
    public LocalDateTime onlineTime;
}
