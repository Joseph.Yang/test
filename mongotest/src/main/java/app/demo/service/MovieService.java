package app.demo.service;

import app.demo.domain.CreateMovieRequest;
import app.demo.domain.Movie;
import app.demo.domain.MovieView;
import app.demo.domain.ReplaceMovieRequest;
import app.demo.domain.UpdateMovieRequest;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import core.framework.inject.Inject;
import core.framework.mongo.MongoCollection;
import core.framework.mongo.Query;
import core.framework.util.Strings;
import core.framework.web.exception.NotFoundException;
import org.bson.conversions.Bson;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author jie
 */
public class MovieService {

    @Inject
    private MongoCollection<Movie> movieMongoCollection;

    public MovieView insert(CreateMovieRequest request) {
        Movie movie = new Movie();
        movie.movieId = request.movieId;
        movie.language = request.language;
        movie.movieMinutes = request.movieMinutes;
        movie.movieName = request.movieName;
        movie.onlineTime = request.onlineTime;
        movieMongoCollection.insert(movie);
        return view(movie);
    }

    public void delete(String movieId) {
        movieMongoCollection.get(movieId).orElseThrow(() -> new NotFoundException("movie not found"));
        movieMongoCollection.delete(movieId);
    }

    public List<MovieView> search(String language, String movieName, Integer movieMinutes) {
        Query query = new Query();
        if (!Strings.isBlank(language)) {
            query.filter = Filters.eq("language", language);
        } else if (movieMinutes != null) {
            query.filter = Filters.eq("movie_minutes", movieMinutes);
        } else if (!Strings.isBlank(movieName)) {
            query.filter = Filters.eq("movie_name", movieName);
        }
        return movieMongoCollection.find(query).stream().map(this::view).collect(Collectors.toList());
    }

    public void replace(ReplaceMovieRequest request) {
        Movie movie = new Movie();
        movie.movieId = request.movieId;
        movie.language = request.language;
        movie.movieMinutes = request.movieMinutes;
        movie.movieName = request.movieName;
        movie.onlineTime = request.onlineTime;
        movieMongoCollection.replace(movie);
    }

    public void update(String movieId, UpdateMovieRequest request) {
        movieMongoCollection.get(movieId).orElseThrow(() -> new NotFoundException("movie not found"));
        Bson filter = Filters.eq("_id", movieId);
        Bson update = Updates.set("movie_name", request.movieName);
        movieMongoCollection.update(filter, update);
    }

    private MovieView view(Movie movie) {
        MovieView movieView = new MovieView();
        movieView.movieId = movie.movieId;
        movieView.language = movie.language;
        movieView.movieMinutes = movie.movieMinutes;
        movieView.movieName = movie.movieName;
        movieView.onlineTime = movie.onlineTime;
        return movieView;
    }
}
