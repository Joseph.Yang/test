package app;

import app.demo.domain.MovieView;
import app.demo.domain.UpdateMovieRequest;
import app.demo.service.MovieService;
import core.framework.module.App;
import core.framework.module.SystemModule;

import java.util.List;


/**
 * @author jie
 */
public class MongoServiceApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));
        load(new MovieModule());
        onStartup(() -> {
            MovieService movieService = bean(MovieService.class);
            movieService.delete("112");

            UpdateMovieRequest request = new UpdateMovieRequest();
            request.movieName = "steve";
            movieService.update("111", request);

            List<MovieView> search = movieService.search("Chinese", null, null);
            search.stream().forEach(System.out::println);

            //movieService.insert();
            //movieService.replace();
        });
    }
}
