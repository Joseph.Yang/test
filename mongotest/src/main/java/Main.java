import app.MongoServiceApp;

/**
 * @author jie
 */
public class Main {
    public static void main(String[] args) {
        new MongoServiceApp().start();
    }
}
