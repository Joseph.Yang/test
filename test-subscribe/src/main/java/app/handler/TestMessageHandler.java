package app.handler;

import app.message.api.TestMessage;
import core.framework.kafka.MessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author joseph
 */
public class TestMessageHandler implements MessageHandler<TestMessage> {
    private final Logger logger = LoggerFactory.getLogger(TestMessageHandler.class);

    @Override
    public void handle(String key, TestMessage value) throws Exception {
        logger.warn("test1" + value.message);
    }
}
